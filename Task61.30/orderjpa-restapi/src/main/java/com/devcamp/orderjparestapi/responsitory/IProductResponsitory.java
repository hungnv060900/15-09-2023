package com.devcamp.orderjparestapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.orderjparestapi.models.Product;

public interface IProductResponsitory extends JpaRepository<Product,Long> {
    
}
