package com.devcamp.orderjparestapi.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.orderjparestapi.models.Order;
import com.devcamp.orderjparestapi.models.Product;
import com.devcamp.orderjparestapi.responsitory.IOrderResponsitory;

@RestController
@RequestMapping("/")
@CrossOrigin
public class OrderController {
    @Autowired
    IOrderResponsitory iOrderResponsitory;

    /**
     * @param order_id
     * @return
     */
    @GetMapping("/devcamp-products")
    public ResponseEntity<Set<Product>> getProductByOrderId(@RequestParam(value = "orderId") long orderId) {
        try {
            Order order = iOrderResponsitory.findById(orderId);

            if (order != null) {
                return ResponseEntity.ok(order.getProducts());
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
