package com.devcamp.orderjparestapi.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.orderjparestapi.models.Customer;
import com.devcamp.orderjparestapi.models.Order;
import com.devcamp.orderjparestapi.responsitory.ICustomerReponsitory;


@RestController
@CrossOrigin
@RequestMapping("/")
public class CustomerOrderController {
    @Autowired
    ICustomerReponsitory iCustomerReponsitory;
    

    @GetMapping("/devcamp-orders")
    public ResponseEntity<Set<Order>> getOrderByCustomerId(@RequestParam(value = "customerId") long customer_id) {
        try {
            Customer vCustomer = iCustomerReponsitory.findById(customer_id);

            if (vCustomer != null) {
                return new ResponseEntity<>(vCustomer.getOrders(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
}
