package com.devcamp.orderjparestapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.orderjparestapi.models.Customer;

public interface ICustomerReponsitory extends JpaRepository<Customer,Long> {
    Customer findById(long id);
}
