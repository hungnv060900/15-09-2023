package com.devcamp.orderjparestapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.orderjparestapi.models.Order;

public interface IOrderResponsitory extends JpaRepository<Order,Long> {
    Order findById(long id);
}
