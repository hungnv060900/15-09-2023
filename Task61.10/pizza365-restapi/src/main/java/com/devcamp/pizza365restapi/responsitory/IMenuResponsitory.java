package com.devcamp.pizza365restapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365restapi.models.CMenu;

public interface IMenuResponsitory extends JpaRepository<CMenu,Long> {
    
}
