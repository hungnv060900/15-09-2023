package com.devcamp.pizza365restapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365restapi.models.Drink;

public interface IDrinkResponsitory extends JpaRepository<Drink,Long> {
    
}
