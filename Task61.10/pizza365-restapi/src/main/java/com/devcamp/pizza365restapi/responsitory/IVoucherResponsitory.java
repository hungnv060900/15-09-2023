package com.devcamp.pizza365restapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365restapi.models.Voucher;

public interface IVoucherResponsitory extends JpaRepository<Voucher,Long> {
    
}
