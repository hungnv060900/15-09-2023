package com.devcamp.pizza365restapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365restapi.models.Customer;

public interface ICustomerResponsitory extends JpaRepository<Customer, Long> {
    Customer findById(long id);
}
