package com.devcamp.pizza365restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pizza365RestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Pizza365RestapiApplication.class, args);
	}

}
