package com.devcamp.pizza365restapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365restapi.models.Order;

public interface IOrderResponsitory extends JpaRepository<Order,Long> {
    
}
